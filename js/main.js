var addHeader = function(h){
    return h;
}

var addImg = function(i){
    return i;
}

var addAd = function(a){
    return a;
}

var renderAd = function(){
    //REQUESTING A PLUMBER

    document.querySelector('#main').innerHTML += addHeader(
        '<h2>Óska eftir pípara</h2>'
    );
    document.querySelector('#main').innerHTML += addImg(
        '<img src="https://skisplumbing.com/wp-content/uploads/2013/05/plumber-main.jpg">'
    );
    document.querySelector('#main').innerHTML += addAd(
        '<p>Allt er stíflað, óska eftir hæfum pípara til að koma öllu í lag sem fyrst!</p>'
    );

    // CAR FOR SALE
    document.querySelector('#ad').innerHTML += addHeader(
        '<h2>Bíll til sölu</h2>'
    
    );
    document.querySelector('#ad').innerHTML += addImg(
        '<img src="bill.jpg">'
    );
    document.querySelector('#ad').innerHTML += addAd(
        '<p>Hér er rosalega flottur bíll til sölu. Hann er keyrður mjög lítið og selst fyrir mjög mikið. Frekari upplýsingar er hægt að fá hjá Sigrúnu í síma 555-5555.</p>'
    );

    //REQUESTING A BIKE

    document.querySelector('#ad').innerHTML += addHeader(
        '<h2>Barnahjól óskast</h2>'
    );
    document.querySelector('#ad').innerHTML += addImg(
        '<img src="bike.jpg">'
    );
    document.querySelector('#ad').innerHTML += addAd(
        '<p>Óska eftir sambærilegu hjóli, verður að vera með hjálpardekkjum. Hugsað fyrir 3+ ára. Er tilbúin til að greiða allt að 5000 kr. Endilega hafið samband í síma 666-6666</p>'
    );
}

renderAd();

module.exports = {
    addHeader,
    addImg,
    addAd,
    renderAd
};